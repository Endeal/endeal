if Meteor.isServer
    Meteor.methods
        closeVendors: (identifier, verifier, provider, type) ->
            @unblock()
            HTTP.post "https://emporium.endeal.me/api/v1/closeVendors/members",
                headers:
                    'x-identity-identifier': identifier
                    'x-identity-verifier': verifier
                    'x-identity-provider': provider
                    'x-identity-type': type
