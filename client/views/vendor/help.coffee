if Meteor.isClient
    Template.vendorHelp.events
        "click #submit": (event, template) ->
            event.preventDefault()
            email = template.find("#identifier").value
            password = template.find("#verifier").value
            template.find("#loading").style.visibility = "visible"
            Meteor.call 'closeVendors', email, password, 'endeal', 'member', (error, results) ->
                if error
                    Materialize.toast error.reason + ': Invalid email/password combination', 4000
                else
                    data = results.data
                    message = data.message
                    Materialize.toast message, 4000
                    $("#emergencyVendorClose").closeModal()
                template.find("#loading").style.visibility = "hidden"
